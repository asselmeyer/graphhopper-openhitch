package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import java.math.BigDecimal
import java.util.*

open class TestData {
    companion object {
        val SECOND=1000;
        val MINUTE=SECOND*60
        val HOUR=MINUTE*60
        val DAY=HOUR*24
        val TIME1 = Date().time+DAY;  // tomorrow
        val TIME2 = TIME1 + TIME1+MINUTE // one minute later
        val DRIVER = User("userIdDriver", "Ferdinand", "Fahrer", "Ferdinand", null)
        val PASSENGER = User("userIdPassenger", "Moritz", "Mitfahrer", "Moritz", null)

        // some points in Berlin
        val P_HAUSBURGSTR = OHPoint.fromLngLat(13.45328,52.52617 );
        val P_TORSTR = OHPoint.fromLngLat(13.3968,52.5287)
        val P_COTHENIUSSTR = OHPoint.fromLngLat( 13.4480129, 52.5282619)
        val P_ZEHDENICKERSTR = OHPoint.fromLngLat( 13.4058858,52.5299841)

        // some driver way in Berlin
        val WAY_DRIVER = Way("idWayDriver", Way.Status.PUBLISHED, DRIVER.id, Role.DRIVER, arrayListOf(P_HAUSBURGSTR, P_TORSTR),TIME1, TIME1+2*HOUR, 1, false);

        // some rider way in Berlin that matches with WAY_DRIVER. The waypoints are not directly on the route of WAY_DRIVER, but close
        val WAY_PASSENGER = Way("idWayPassenger", Way.Status.PUBLISHED, PASSENGER.id, Role.PASSENGER, arrayListOf(P_COTHENIUSSTR, P_ZEHDENICKERSTR),TIME2, TIME1+2*HOUR,1, false);

        // same as WAY_DRIVER, but with a different id and one day later
        val WAY2_DRIVER = WAY_DRIVER.copy(id="idWay2Driver",startTime= TIME1+DAY,endTime=TIME1+DAY+2*HOUR)

        // Lift resulting from matching WAY_DRIVER, WAY_PASSENGER. Values are only given approximately.
        // This is not intended for comparison with real geo-information
        val LIFT1 = Lift(
                id = Util.randomString(),
                status = Lift.Status.SUGGESTED,
                passengerWayId = WAY_PASSENGER.id,
                driverWayId = WAY_DRIVER.id,
                pickUpPoint = P_COTHENIUSSTR,
                pickUpHint = BigDecimal(3),
                dropOffPoint = P_ZEHDENICKERSTR,
                dropOffHint = BigDecimal(6),
                usefulness = 3.4,
                sharedDistance = 3.4,
                pickupTime = TIME1+5*60*1000,  // 5 minutes after TIME1
                price = BigDecimal(1),
                currency = "EUR",
                deviationDistance = 0.2,
                deviationTime = 1000)
    }
}