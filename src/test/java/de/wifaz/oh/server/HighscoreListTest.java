package de.wifaz.oh.server;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class HighscoreListTest {

    class IntegerComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer a, Integer b) {
            return Integer.compare(a,b);
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }

    @Test
    void add() {
        HighscoreList<Integer> hslist = new HighscoreList<>(3, new IntegerComparator());

        assertEquals(0, hslist.size());
        boolean b = hslist.add(5);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(5));

        b = hslist.add(1);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(1,5));

        b = hslist.add(7);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(1,5,7));

        b = hslist.add(10);
        assertFalse(b);
        assertEquals(hslist, Arrays.asList(1,5,7));

        b = hslist.add(3);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(1,3,5));

        b = hslist.add(4);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(1,3,4));

        b = hslist.add(0);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(0,1,3));

        b = hslist.add(1);
        assertTrue(b);
        assertEquals(hslist, Arrays.asList(0,1,1));
    }
}