package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import org.junit.Before
import org.junit.Rule
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import java.math.BigDecimal
import java.util.*

internal class HitchDBTest : TestData() {
    fun getDb(): HitchDB {
        //return RamHitchDB();

        return MongoHitchDB()
    }

    @Test
    fun crudWay() {
        var db = getDb()
        assertFalse(db.hasWay(WAY_DRIVER.id))

        db.putWay(WAY_DRIVER)
        assertTrue(db.hasWay(WAY_DRIVER.id))

        var w = db.getWay(WAY_DRIVER.id)
        assertNotNull(w)
        assertNull(db.getWay("UnusedId"))
        assertEquals(WAY_DRIVER.id,w?.id)
        assertEquals(WAY_DRIVER.startTime,w?.startTime)

        var ways = db.getAllWays()
        assertEquals(1,ways.size)
        w = ways[0]
        assertNotNull(w)
        assertEquals(WAY_DRIVER.id,w?.id)
        assertEquals(WAY_DRIVER.startTime,w?.startTime)

        db.removeWay(WAY_DRIVER.id)
        assertFalse(db.hasWay(WAY_DRIVER.id))

        assertTrue(db.isClean())
    }

    @Test
    fun crudLift() {
        var db = getDb()
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        assertNull(db.getLift(LIFT1.id))
        db.putLift(LIFT1)
        var lift = db.getLift(LIFT1.id)
        assertNotNull(lift)
        assertEquals(LIFT1.id,lift?.id)
        assertEquals(WAY_DRIVER.id,lift?.driverWayId)
        assertEquals(WAY_PASSENGER.id,lift?.passengerWayId)
        db.removeLift(LIFT1)
        assertNull(db.getLift(LIFT1.id))

        db.removeWay(WAY_DRIVER.id)
        db.removeWay(WAY_PASSENGER.id)
        assertTrue(db.isClean())
    }

    @Test
    fun wayStatusUpdateRetainsLift() {
        var db = getDb()
        db.putWay(WAY_DRIVER)
        assertEquals(WAY_DRIVER,db.getWay(WAY_DRIVER.id))
        db.putWay(WAY_PASSENGER)
        db.putLift(LIFT1)
        val newWay = WAY_DRIVER.copy(status = Way.Status.STARTED)
        db.putWay(newWay)
        assertEquals(newWay,db.getWay(WAY_DRIVER.id))
        assertEquals(LIFT1,db.getLift(LIFT1.id))
    }

    @Test
    fun autoRemoveLift() {
        var db = getDb()
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        assertNull(db.getLift(LIFT1.id))
        db.putLift(LIFT1)
        db.removeWay(WAY_DRIVER.id)
        assertNull(db.getLift(LIFT1.id))
    }

    @Test
    fun callListener() {
        var db = getDb()
        val listenerMock: HitchDB.Listener = mock(HitchDB.Listener::class.java)
        db.addListener(listenerMock)
        db.putWay(WAY_DRIVER)
        db.putWay(WAY_PASSENGER)
        db.putLift(LIFT1)
        db.removeWay(WAY_DRIVER.id)
        verify(listenerMock).onRemoveLift(LIFT1)
    }

}