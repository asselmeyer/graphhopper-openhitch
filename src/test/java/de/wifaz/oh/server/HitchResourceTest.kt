package de.wifaz.oh.server

import com.nhaarman.mockitokotlin2.check
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.wifaz.oh.protocol.*
import junit.framework.Assert.*
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import java.net.HttpURLConnection
import java.security.Principal
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext


internal class HitchResourceTest : TestData() {
    @Test
    fun createWay() {
        val db : HitchDB = getDB()
        val messengerMock : Messenger = mock()
        val matcherMock : Matcher = mock()

        val hitchResource = HitchResource(db,messengerMock,matcherMock)

        // create some way
        var response: Response = hitchResource.createWay(WAY_DRIVER)
        assertEquals(HttpURLConnection.HTTP_OK,response.status)  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)

        // create an other, non conflicting, non interacting way
        response = hitchResource.createWay(WAY2_DRIVER)
        assertEquals(HttpURLConnection.HTTP_OK,response.status)  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)
    }

    @Test
    fun deleteWay() {
        val db : HitchDB = getDB()
        val messengerMock : Messenger = mock()
        val matcherMock : Matcher = mock()


        // Mock two different SecurityContexts to mimic access of two different users
        // USER1
        val securityContextMock1 = getSecurityContextMock(DRIVER)
        val securityContextMock2 = getSecurityContextMock(PASSENGER)

        val hitchResource = HitchResource(db,messengerMock,matcherMock)

        // create some way as USER1
        var response: Response = hitchResource.createWay(WAY_DRIVER)
        assertEquals(HttpURLConnection.HTTP_OK,response.status)  // FIXME should become HTTP_CREATED when using Method CREATE
        verifyZeroInteractions(messengerMock)

        // fail deleting it as USER2
        try {
            hitchResource.deleteWay(WAY_DRIVER.id,securityContextMock2)
            fail()
        } catch (exc:WebApplicationException) {}
        assertTrue(db.hasWay(WAY_DRIVER.id))

        // delete it
        response = hitchResource.deleteWay(WAY_DRIVER.id,securityContextMock1)
        assertEquals(HttpURLConnection.HTTP_OK,response.status)  // FIXME should be HTTP_NO_CONTENT
        assertFalse(db.hasWay(WAY_DRIVER.id))

        verifyZeroInteractions(messengerMock)
    }

    private fun getSecurityContextMock(user: User): SecurityContext {
        val securityContextMock: SecurityContext = mock()
        val principalMock: Principal = mock()
        whenever(securityContextMock.userPrincipal).thenReturn(principalMock)
        whenever(principalMock.name).thenReturn(user.id)
        return securityContextMock
    }

    @Test
    fun match() {
        val db : HitchDB = getDB()
        val messengerMock : Messenger = mock()
        val matcherMock : Matcher = mock()

        whenever(matcherMock.match(WAY_PASSENGER, WAY_DRIVER)).thenReturn(LIFT1)

        val hitchResource = HitchResource(db,messengerMock,matcherMock)

        hitchResource.createWay(WAY_DRIVER)
        hitchResource.createWay(WAY_PASSENGER)

        val userIdCaptor = ArgumentCaptor.forClass(String::class.java)
        val messageCaptor = ArgumentCaptor.forClass(HitchMessage::class.java)
        checkReceivedExactlyLIFT1(messengerMock)
    }

    private fun checkReceivedExactlyLIFT1(messengerMock: Messenger) {
        verify(messengerMock, atMost(1)).send(
                check { assertEquals(WAY_PASSENGER.userId, it) },
                check {
                    assertEquals(WAY_PASSENGER.id, it.referenceWayId)
                    assertEquals(LiftInfoMessage::class.java, it.javaClass)
                    val lim = it as LiftInfoMessage
                    assertEquals(1, lim.liftInfos.size)
                    val liftInfo = lim.liftInfos[0]
                    assertEquals(LIFT1, liftInfo.lift)
                }
        );
        verifyNoMoreInteractions(messengerMock)
    }

    class DebugMessenger : Messenger {

        override fun close() {
        }

        override fun send(userId: String, payload: HitchMessage?) {
            System.out.println(String.format("send(%s,%s)",userId,HitchMessage::class.java.name))
        }

    }

    @Test
    fun normalStatusPromotion() {
        val db : HitchDB = getDB()
        val messengerMock : Messenger = mock()
        val matcherMock : Matcher = mock()

        // Mock match to get a lift into the database
        whenever(matcherMock.match(WAY_PASSENGER, WAY_DRIVER)).thenReturn(LIFT1)
        val debugMessenger = DebugMessenger()
        val hitchResource = HitchResource(db,messengerMock,matcherMock)
        // val hitchResource = HitchResource(db,debugMessenger,matcherMock)
        hitchResource.createWay(WAY_DRIVER)
        hitchResource.createWay(WAY_PASSENGER)
        checkReceivedExactlyLIFT1(messengerMock)
        reset(messengerMock)

        val securityContextMockPassenger = getSecurityContextMock(PASSENGER)
        hitchResource.updateLiftStatus(LIFT1.id, Lift.Status.REQUESTED, securityContextMockPassenger)

        verify(messengerMock).send(
                check {
                    assertEquals(DRIVER.id, it)
                },
                check {
                    assertEquals(WAY_DRIVER.id, it.referenceWayId)
                    assertEquals(LiftInfoMessage::class.java, it.javaClass)
                    val lim = it as LiftInfoMessage
                    assertEquals(lim.referenceWayId, WAY_DRIVER.id)
                    assertEquals(1, lim.liftInfos.size)
                    val liftInfo = lim.liftInfos[0]
                    assertEquals(LIFT1.copy(status = Lift.Status.REQUESTED),liftInfo.lift)
                }
        );
        verifyNoMoreInteractions(messengerMock)

        reset(messengerMock)

        val securityContextMockDriver = getSecurityContextMock(DRIVER)
        hitchResource.updateLiftStatus(LIFT1.id, Lift.Status.ACCEPTED, securityContextMockDriver)
        verify(messengerMock).send(
                check { assertEquals(PASSENGER.id, it) },
                check {
                    assertEquals(WAY_PASSENGER.id, it.referenceWayId)
                    assertEquals(LiftStatusMessage::class.java, it.javaClass)
                    val lsm = it as LiftStatusMessage
                    assertEquals(Lift.Status.ACCEPTED, lsm.status)
                    assertEquals(LIFT1.id,lsm.liftId)
                }
        );
        verifyNoMoreInteractions(messengerMock)
    }

    @Test
    fun onRemoveLift() {
    }

    private fun getDB() : HitchDB {
        var db = RamHitchDB()
        db.putUser(DRIVER)
        db.putUser(PASSENGER)
        return db
    }
}