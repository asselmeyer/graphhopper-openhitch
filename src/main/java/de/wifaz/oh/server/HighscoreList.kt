package de.wifaz.oh.server

import java.util.*

class HighscoreList<T>(private val capacity: Int, private val comparator: Comparator<in T>) : ArrayList<T>(capacity) {
    override fun add(e: T): Boolean {
        var index = Collections.binarySearch(this, e, comparator)
        if (index < 0) {
            index = index.inv()
        }
        assert(index <= capacity)
        if (index == capacity) {
            return false
        }
        assert(size <= capacity)
        if (size == capacity) {
            removeAt(capacity - 1)
        }
        super.add(index, e)
        return true
    }

}