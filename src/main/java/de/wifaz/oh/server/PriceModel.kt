package de.wifaz.oh.server

import sun.reflect.generics.reflectiveObjects.NotImplementedException
import java.math.BigDecimal
import java.util.*

interface PriceModel {
    val currency: Currency
    fun getPrice(sharedDistance: Double, deviationDistance: Double): BigDecimal

    companion object {
        @JvmOverloads
        fun selectPriceModel(locale: Locale = Locale.GERMAN): PriceModel {
            return if (locale == Locale.GERMAN) {
                BasePriceModel(Currency.getInstance("EUR"), 1.0, 0.08, 0.32)
            } else {
                throw NotImplementedException()
            }
        }
    }
}