package de.wifaz.oh.server

import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.User
import de.wifaz.oh.protocol.Way
import javax.inject.Singleton

@Singleton
interface HitchDB {
    interface Listener {
        fun onRemoveLift(lift: Lift)
    }

    fun putUser(user: User)
    fun removeUser(userId:String)
    fun getLift(liftId: String): Lift?
    fun addListener(listener: Listener)
    fun putLift(lift: Lift)
    fun addMatch(lift: Lift): Boolean
    fun removeLift(lift: Lift)
    fun putWay(way: Way)
    fun hasWay(wayId: String): Boolean
    fun removeWay(wayId: String)
    fun getAllWays(): List<Way>
    fun getUser(userId: String): User?
    fun getWay(wayId: String): Way?
    fun getLifts(wayId: String) : List<Lift>
    fun getBestMatches(passengerWayId: String): List<Lift>

    /**
     * For testing only. Returns true if the database is empty and there is no memory garbage
     */
    fun isClean() :Boolean
}