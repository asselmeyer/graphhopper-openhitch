package de.wifaz.oh.server

import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Way

interface Matcher {
    fun match(wayPassenger: Way, wayDriver: Way) : Lift?
}