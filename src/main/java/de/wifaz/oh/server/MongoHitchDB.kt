package de.wifaz.oh.server

import com.mongodb.client.MongoCollection
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.User
import de.wifaz.oh.protocol.Way
import org.litote.kmongo.KMongo
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import org.litote.kmongo.getCollection

class MongoHitchDB : HitchDB {
    val col : MongoCollection<Way>
    init {
        val client = KMongo.createClient() //get com.mongodb.MongoClient new instance
        val database = client.getDatabase("test") //normal java driver usage
        col = database.getCollection<Way>() //KMongo extension method
    }

    override fun putUser(user: User) {
        TODO("Not yet implemented")
    }

    override fun removeUser(userId: String) {
        TODO("Not yet implemented")
    }

    override fun getLift(liftId: String): Lift? {
        TODO("Not yet implemented")
    }

    override fun addListener(listener: HitchDB.Listener) {
        TODO("Not yet implemented")
    }

    override fun putLift(lift: Lift) {
        TODO("Not yet implemented")
    }

    override fun addMatch(lift: Lift): Boolean {
        TODO("Not yet implemented")
    }

    override fun removeLift(lift: Lift) {
        TODO("Not yet implemented")
    }

    override fun putWay(way: Way) {
        col.insertOne(way)
    }

    override fun hasWay(wayId: String): Boolean {
        return getWay(wayId)!=null
    }

    override fun removeWay(wayId: String) {
        col.deleteOne(Way::id eq wayId)
    }

    override fun getAllWays(): List<Way> {
        val result = ArrayList<Way>()
        col.find().forEach {
            result.add(it)
        }
        return result
    }

    override fun getUser(userId: String): User? {
        TODO("Not yet implemented")
    }

    override fun getWay(wayId: String): Way? {
        return col.findOne( Way::id eq wayId )
    }

    override fun getLifts(wayId: String): List<Lift> {
        TODO("Not yet implemented")
    }

    override fun getBestMatches(passengerWayId: String): List<Lift> {
        TODO("Not yet implemented")
    }

    override fun isClean(): Boolean {
        return col.find().count()==0
    }

}