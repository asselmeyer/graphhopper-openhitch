package de.wifaz.oh.server

import de.wifaz.oh.protocol.*
import org.slf4j.LoggerFactory
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.Path
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Central storage for hitch data, in particular the advertized ways
 *
 * FIXME for now, all information resides in RAM and is lost, when the server is shut down
 * we need some redundant cloud storage instead
 *
 * FIXME: Annotation @Path was added to be able to environment.jersey().register(HitchDB.class) in GraphHppperBundle.java
 * This is clearly wrong. what is the right way to do it?
 * This might help:
 * https://stackoverflow.com/questions/38976724/how-to-set-an-object-to-context-so-that-i-can-get-it-anywhere-in-the-application
 */
@Singleton
class RamHitchDB @Inject public constructor() : HitchDB  {
    companion object {
        var debugCounter = 0
    }

    private val users: MutableMap<String, User> = HashMap()
    private val lifts: MutableMap<String, Lift> = HashMap()
    private val wayContexts: MutableMap<String, WayContext?> = HashMap()
    private val listeners: MutableList<HitchDB.Listener> = ArrayList()
    override fun putUser(user: User) {
        users.put(user.id,user)
    }

    override fun removeUser(userId: String) {
        users.remove(userId)
    }

    override fun getLift(liftId: String): Lift? {
        return lifts[liftId]
    }

    // FIXME do we need removeListener?
    override fun addListener(listener: HitchDB.Listener) {
        listeners.add(listener)
    }

    override fun putLift(lift: Lift) {
        lifts[lift.id] = lift
        wayContexts[lift.driverWayId]!!.lifts[lift.id] = lift
        wayContexts[lift.passengerWayId]!!.lifts[lift.id] = lift
    }

    override fun removeLift(lift: Lift) {
        for (listener in listeners) {
            listener.onRemoveLift(lift)
        }
        val id = lift.id
        lifts.remove(id)
        wayContexts[lift.driverWayId]!!.lifts.remove(id)
        wayContexts[lift.passengerWayId]!!.lifts.remove(id)
    }

    override fun putWay(way: Way) {
        val wayContext = wayContexts[way.id]
        if (wayContext==null) {
            wayContexts[way.id] = WayContext(way)
        } else {
            wayContext.way = way
        }
    }

    override fun hasWay(wayId: String): Boolean {
        return wayContexts.containsKey(wayId)
    }

    override fun removeWay(wayId: String) {
        val wayContext = wayContexts[wayId]
        for (lift in wayContext!!.lifts.values) {
            removeLift(lift)
        }
        wayContexts.remove(wayId)
    }

    override fun getAllWays() : ArrayList<Way> {
        val result = ArrayList<Way>(wayContexts.size)
        for (wayContext in wayContexts.values) {
            result.add(wayContext!!.way)
        }
        return result
    }

    override fun getUser(userId: String): User? {
        return users[userId]
    }

    override fun getWay(wayId: String): Way? {
        return wayContexts[wayId]?.way;
    }

    override fun addMatch(lift: Lift): Boolean {
        val bestMatches = wayContexts[lift.passengerWayId]!!.bestMatches!!
        return bestMatches.add(lift)
    }

    override fun getBestMatches(passengerWayId: String): List<Lift> {
        return wayContexts[passengerWayId]!!.bestMatches!!
    }

    override fun isClean(): Boolean {
        // for now, ignore users and listeners
        return lifts.isEmpty() && wayContexts.isEmpty();
    }

    override fun getLifts(wayId: String) : List<Lift> {
        val wayContext = wayContexts.get(wayId)
        return if (wayContext==null) {
            emptyList<Lift>();
        } else {
            ArrayList(wayContext.lifts.values)
        }
    }

    init {
        val logger = LoggerFactory.getLogger(HitchResource::class.java)
    }
}

private class WayContext(
    var way: Way
) {
    val lifts: MutableMap<String, Lift> = HashMap()
    /**
     * Passenger ways: Best matches
     * Driver ways: null
     */
    var bestMatches: HighscoreList<Lift>? = null

    private inner class LiftComparator : Comparator<Lift> {
        override fun compare(a: Lift, b: Lift): Int {
            return java.lang.Double.compare(a.usefulness, b.usefulness)
        }
    }

    init {
        if (way.role == Role.PASSENGER) {
            bestMatches = HighscoreList(HitchInterface.MAX_OFFERS, LiftComparator())
        }
    }
}