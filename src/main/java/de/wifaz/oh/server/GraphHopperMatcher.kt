package de.wifaz.oh.server

import com.graphhopper.GHRequest
import com.graphhopper.GraphHopperAPI
import com.graphhopper.PathWrapper
import com.graphhopper.util.Parameters
import com.graphhopper.util.PointList
import com.graphhopper.util.shapes.GHPoint
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Way
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class GraphHopperMatcher @Inject constructor(
    private val graphHopper: GraphHopperAPI
) : Matcher {
    override fun match(wayPassenger: Way, wayDriver: Way): Lift? {
        return Scout(graphHopper,wayPassenger,wayDriver).match()
    }
}

private class Scout internal constructor(private val graphHopper: GraphHopperAPI, private val wayPassenger: Way, private val wayDriver: Way) {
    private val wayPointsPassenger: List<GHPoint>
    private val wayPointsDriver: List<GHPoint>
    // FIXME cache these two in WayContext
    fun match() : Lift? {
        val pathDirect = getPath(wayPointsDriver[0], wayPointsDriver[1])
        val timeDirect = pathDirect.time
        val distanceDirect = pathDirect.distance
        // FIXME cache these two in WayContext
        val pathToPickup = getPath(wayPointsDriver[0], wayPointsPassenger[0])
        val pathFromPickupToDriverDestination = getPath(wayPointsPassenger[0], wayPointsDriver[1])
        val timeWithPickup = pathToPickup.time + pathFromPickupToDriverDestination.time
        val distanceWithPickup = pathToPickup.distance + pathFromPickupToDriverDestination.distance
        val deviationTime = timeWithPickup - timeDirect
        val deviationDistance = distanceWithPickup - distanceDirect
        // FIXME remove constants MAX_DEVIATION_TIME etc and deliver the information in the Way object
        if (deviationTime > MAX_DEVIATION_TIME || deviationDistance > MAX_DEVIATION_DISTANCE) {
            return null
        }
        val sharedPath = sharedPath() ?: return null
        val sharedDistance = sharedPath.distance
        if (sharedDistance == 0.0 || deviationDistance / sharedDistance > MAX_DEVIATION_DISTANCE_RATIO) {
            return null
        }
        val timeToPickup = pathToPickup.time
        val pickupTime = wayDriver.startTime + timeToPickup
        val priceModel = PriceModel.selectPriceModel()
        val price = priceModel.getPrice(sharedDistance, deviationDistance)
        // Ok, Path is good enough, return LiftOffer
        val points = sharedPath.points
        return Lift(
                id = Util.randomString(),
                status = Lift.Status.SUGGESTED,
                passengerWayId = wayPassenger.id,
                driverWayId = wayDriver.id,
                pickUpPoint = wayPassenger.waypoints[0],
                pickUpHint = BigDecimal(3),
                dropOffPoint = Util.ghPoint2ohPoint(points[points.size - 1]),
                dropOffHint = BigDecimal(6),
                usefulness = sharedDistance,
                sharedDistance = sharedDistance,
                pickupTime = pickupTime,
                price = price,
                currency = priceModel.currency.currencyCode,
                deviationDistance = deviationDistance,
                deviationTime = deviationTime)
    }

    //  Actually, the shared Path is already calculated. It consists of the first matchLength points in passengerPoints/driverPoints
    // Unfortunately, the graphhopper-api only delivers the points, not the time or distance, so we have to calculate the route again
    private fun sharedPath() : PathWrapper? {
        val driverPathAfterPickup = getPath(Arrays.asList(wayPointsPassenger[0], wayPointsDriver[1]), true)
        val passengerPath = getPath(Arrays.asList(wayPointsPassenger[0], wayPointsDriver[1]), true)
        val driverPoints = driverPathAfterPickup.points
        val passengerPoints = passengerPath.points
        val matchLength = getMatchLength(driverPoints, passengerPoints)
        return if (matchLength <= 0) {
            null
        } else getPath(passengerPoints[0], passengerPoints[matchLength])
    }

    private fun getMatchLength(p: PointList, q: PointList): Int {
        val nPoints = q.size
        var i = 0
        while (i < nPoints && q[i] == p[i]) {
            i++
        }
        if (i == 0) {
            logger.error("Something's wrong here. The two ways should start at the same point.")
        }
        return i - 1
    }

    private fun getPath(vararg ghPoints: GHPoint): PathWrapper {
        return getPath(Arrays.asList(*ghPoints), false)
    }

    private fun getPath(ghPoints: List<GHPoint>, calcPoints: Boolean): PathWrapper {
        val requestPoints: List<GHPoint> = ArrayList(ghPoints)
        val request = GHRequest(requestPoints)
        request.setVehicle("car").hints.put(Parameters.Routing.CALC_POINTS, calcPoints)
        val response = graphHopper.route(request)
        return response.best
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HitchResource::class.java)
        private const val MAX_DEVIATION_TIME = 3 * 60 * 1000.toLong()
        private const val MAX_DEVIATION_DISTANCE_RATIO = 0.5
        private const val MAX_DEVIATION_DISTANCE = 2000.0
    }

    init {
        wayPointsPassenger = Util.ohPoints2ghPoints(wayPassenger.waypoints)
        wayPointsDriver = Util.ohPoints2ghPoints(wayDriver.waypoints)
    }
}