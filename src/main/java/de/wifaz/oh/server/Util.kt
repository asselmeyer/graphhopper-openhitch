package de.wifaz.oh.server

import com.graphhopper.util.shapes.GHPoint
import de.wifaz.oh.protocol.OHPoint
import java.security.SecureRandom
import java.util.*

object Util {
    private const val base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    private const val randomIdLength = 27 //
    private val random: Random = SecureRandom()
    /**
     * Create random id as a string
     *
     * @return random String of 27 base64 characters (=entropy 162 bit)
     */
    @JvmStatic
    fun randomString(): String {
        val sb = StringBuilder(randomIdLength)
        for (i in 0 until randomIdLength) sb.append(base64chars[random.nextInt(base64chars.length)])
        return sb.toString()
    }

    fun ohPoints2ghPoints(ohPoints: List<OHPoint>): ArrayList<GHPoint> {
        val result = ArrayList<GHPoint>(ohPoints.size)
        for (ohp in ohPoints) {
            result.add(ohPoint2ghPoint(ohp))
        }
        return result
    }

    fun ohPoint2ghPoint(ohp: OHPoint): GHPoint {
        return GHPoint(ohp.latitude, ohp.longitude)
    }

    fun ohPoints2String(ghPoints: List<GHPoint>): String {
        val sj = StringJoiner(", ")
        for (ghp in ghPoints) {
            sj.add("(" + ghp.getLat() + "," + ghp.getLon() + ")")
        }
        return sj.toString()
    }

    fun ghPoint2ohPoint(ghp: GHPoint): OHPoint {
        return OHPoint.fromLngLat(ghp.getLon(), ghp.getLat())
    }

    fun <T> getLast(list: List<T>): T {
        return list[list.size - 1]
    }
}