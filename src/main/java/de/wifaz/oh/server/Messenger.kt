package de.wifaz.oh.server

import de.wifaz.oh.protocol.HitchMessage
import javax.inject.Singleton

@Singleton
interface Messenger {
    fun close()
    fun send(userId: String, payload: HitchMessage?)
}