/*
 * (c) Max kubierschky
 *
 * Max kubierschky licenses this file to you under the Server Side Public License,
 *  Version 1 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       https://www.mongodb.com/licensing/server-side-public-license
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
/*
 * (c) Max Kubierschky
 *
 * Max Kubierschky licenses this file to you under the Server Side Public License,
 *  Version 1 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *
 *       https://www.mongodb.com/licensing/server-side-public-license
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.wifaz.oh.server

import com.fasterxml.jackson.databind.ObjectMapper
import com.graphhopper.GraphHopperAPI
import de.wifaz.oh.protocol.*
import org.slf4j.LoggerFactory
import java.net.HttpURLConnection
import java.util.*
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import kotlin.collections.ArrayList

@Path("hitch")
class HitchResource @Inject constructor(private val db: HitchDB,
                                        private val messenger: Messenger,
                                        private val matcher: Matcher
                                        ) : HitchDB.Listener {
    @PUT
    @Secured
    @Path("ways/create")
    @Consumes(MediaType.APPLICATION_JSON)
    fun createWay(way: Way): Response {
        // FIXME brauche Eingangsprüfung für Weg
        // FIXME nur user darf Wege für sich selbst anlegen
        val wayId = way.id
        if (db.hasWay(wayId)) {
            throw WebApplicationException("Invalid create request, duplicate way id: $way", HttpURLConnection.HTTP_CONFLICT)
        }
        logger.info("create: $way")
        db.putWay(way)
        match(way)
        return Response.ok().build()
    }

    private fun match(way: Way) { // FIXME: perform asynchronously
        if (way.waypoints.size != 2) {
            logger.error("way with stopover (wayId=" + way.id + "). Not yet implemented")
            // FIXME implement for drivers, block for passengers
            return
        }
        when (way.role) {
            Role.PASSENGER -> matchPassenger(way)
            Role.DRIVER -> matchDriver(way)
        }
    }

    private fun matchDriver(wayDriver: Way) {
        for (wayPassenger in db.getAllWays()) {
            if (wayPassenger.role != Role.PASSENGER) {
                continue
            }
            if (wayPassenger.waypoints.size != 2) {
                logger.error("Passenger way with stopover (wayId=" + wayPassenger.id + ").")
                continue
            }
            val lift = matcher.match(wayPassenger, wayDriver)
            if (lift != null) {
                val isHighscore = db.addMatch(lift)
                if (isHighscore) {
                    val driver = db.getUser(wayDriver.id)
                    if (driver == null) {
                        logger.error("Unknown user id")
                    } else {
                        val status = Lift.Status.SUGGESTED
                        val offer = LiftInfo(
                            lift = lift,
                            role = Role.PASSENGER,
                            partner = driver,
                            partnerWay = wayDriver
                        )
                        val message = LiftInfoMessage(wayPassenger.id, ArrayList(setOf(offer)))
                        messenger.send(wayPassenger.userId, message)
                        db.putLift(lift)
                    }
                }
            }
        }
    }

    private fun matchPassenger(wayPassenger: Way) {
        for (wayDriver in db.getAllWays()) {
            if (wayDriver.role != Role.DRIVER) {
                continue
            }
            if (wayDriver.waypoints.size != 2) {
                logger.error("Way with stopover (wayId=" + wayDriver.id + "). Not yet implemented")
                // FIXME implement
                continue
            }
            val lift = matcher.match(wayPassenger, wayDriver)?.let {
                db.addMatch(it)
            }
        }
        // Note: We send a message in any case. If we didn't find anything, we send an empty list
// such that the client can inform the user about the zero result.
        val bestMatches = db.getBestMatches(wayPassenger.id)
        val message = createLiftOfferMessage(wayPassenger.id, bestMatches)
        messenger.send(wayPassenger.userId, message)
        for (lift in bestMatches) {
            db.putLift(lift)
        }
    }

    private fun createLiftOfferMessage(passengerWayId: String, bestMatches: List<Lift>): HitchMessage {
        val offers = ArrayList<LiftInfo>()
        for (lift in bestMatches) {
            val driverWay = db.getWay(lift.driverWayId)
            if (driverWay == null) {
                logger.error("Unknown way id")
                continue
            }
            val driver = db.getUser(driverWay.userId)
            if (driver == null) {
                logger.error("Unknown user id")
                continue
            }
            val liftInfo = LiftInfo(
                lift = lift,
                role = Role.PASSENGER,
                partner = driver,
                partnerWay = driverWay
            )
            offers.add(liftInfo)
        }
        return LiftInfoMessage(passengerWayId,offers);
    }

    @DELETE
    @Secured
    @Path("way/{wayId}")
    fun deleteWay(@PathParam("wayId") wayId: String, @Context securityContext: SecurityContext): Response {
        val username = securityContext.userPrincipal.name
        logger.debug("username=$username")
        return try {
            val userId = db.getWay(wayId)?.userId
                    ?: throw WebApplicationException("Invalid cancel request, wayId not listed: $wayId", HttpURLConnection.HTTP_BAD_REQUEST)
            if (userId != username) {
                logger.error(String.format("User %s tried to delete way of user %s", username, userId))
                throw WebApplicationException("You can only delete your own ways", HttpURLConnection.HTTP_FORBIDDEN)
            }
            logger.info("cancel: $wayId")
            db.removeWay(wayId)
            Response.ok().build()
        } catch (e: IllegalArgumentException) {
            throw WebApplicationException(HttpURLConnection.HTTP_BAD_REQUEST)
        }
    }

    // FIXME only publish in debugging version
    @GET
    @Path("monitorInfo")
    @Produces(MediaType.APPLICATION_JSON)
    fun monitorInfo(): MonitorInfo {
        logger.debug("Enter monitorInfo")
        val mapper = ObjectMapper()
        logger.debug("###Listing Ways")
        val ways = db.getAllWays()
        var lifts = HashSet<Lift>()
        for (way in ways) {
            logger.debug("way: "+mapper.writeValueAsString(way))
            for (lift in db.getLifts(way.id)) {
                 lifts.add(lift)
                logger.debug("lift: "+mapper.writeValueAsString(lift))
            }
        }

        val monitorInfo = MonitorInfo(ArrayList(ways), ArrayList(lifts))
        return monitorInfo
    }

    private fun getRole(lift: Lift, userId: String): Role? {
        val passengerWay = db.getWay(lift.passengerWayId)!!
        if (passengerWay.userId == userId) {
            return Role.PASSENGER
        }
        val driverWay = db.getWay(lift.driverWayId)!!
        if (driverWay.userId == userId) {
            return Role.DRIVER
        }
        return null
    }

    @PUT
    @Secured
    @Path("way/{wayId}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateWayStatus(@PathParam("wayId") wayId: String, newStatus: Way.Status, @Context securityContext: SecurityContext): Response {
        val username = securityContext.userPrincipal.name
        logger.debug("username=$username")
        val way = db.getWay(wayId)
                ?: throw WebApplicationException("Invalid update request, wayId not listed: $wayId", HttpURLConnection.HTTP_BAD_REQUEST)
        val ok = newStatus > way.status
        if (!ok) {
            val message = String.format("Invalid update request, status transition not allowed: %s, %s",
                    way.status.name, newStatus.name)
            throw WebApplicationException(message, HttpURLConnection.HTTP_FORBIDDEN)
        }

        val newWay = way.copy(status=newStatus)
        db.putWay(newWay)
        informPartneraOfWayStatusChange(newWay, username)
        return Response.ok().build()
    }

    @PUT
    @Secured
    @Path("lift/{liftId}/status")
    @Consumes(MediaType.APPLICATION_JSON)
    fun updateLiftStatus(@PathParam("liftId") liftId: String, newStatus: Lift.Status, @Context securityContext: SecurityContext): Response {
        val username = securityContext.userPrincipal.name
        logger.debug("username=$username")
        val lift = db.getLift(liftId)
                ?: throw WebApplicationException("Invalid update request, liftId not listed: $liftId", HttpURLConnection.HTTP_BAD_REQUEST)
        val oldStatus = lift.status
        val role = getRole(lift, username)
                ?: throw WebApplicationException("Invalid update request, lift doesn't involve user: liftId=$liftId, userId=$username", HttpURLConnection.HTTP_BAD_REQUEST)
        val ok = ( oldStatus == Lift.Status.SUGGESTED && newStatus == Lift.Status.REQUESTED && role == Role.PASSENGER
                || oldStatus == Lift.Status.REQUESTED && newStatus == Lift.Status.ACCEPTED && role == Role.DRIVER
                || oldStatus == Lift.Status.ACCEPTED && newStatus == Lift.Status.PASSENGER_CANCELED && role == Role.PASSENGER
                || oldStatus == Lift.Status.ACCEPTED && newStatus == Lift.Status.DRIVER_CANCELED && role == Role.DRIVER)
        return if (ok) {
            val oldStatus = lift.status
            val newLift = lift.copy(status = newStatus)
            db.putLift(newLift)
            informPartnerOfLiftStatusChange(newLift, oldStatus, username, role)
            Response.ok().build()
        } else {
            val message = String.format("Invalid update request, status transition not allowed: %s, %s, %s",
                    oldStatus.name, newStatus.name, role.name)
            throw WebApplicationException(message, HttpURLConnection.HTTP_FORBIDDEN)
        }
    }

    private fun informPartneraOfWayStatusChange(way: Way, username: String?) {
        for (lift in db.getLifts(way.id)) {
            val message = WayStatusMessage(
                    wayId = way.id,
                    status = way.status)
            val partnerId : String? = getPartnerId(way,lift)
            if(partnerId==null) {
                logger.error("Inconsistent database, no partner way found for lift ${lift.id} (way ${way.id}")
                continue
            }
            messenger.send(partnerId, message)
        }
    }

    private fun getPartnerId(way: Way, lift: Lift): String? {
        return when(way.role) {
            Role.DRIVER -> db.getWay(lift.passengerWayId)?.id
            Role.PASSENGER -> db.getWay(lift.driverWayId)?.id
        }
    }

    private fun informPartnerOfLiftStatusChange(newLift: Lift, oldStatus: Lift.Status, issuerId: String, issuerRole: Role) {
        val newStatus = newLift.status
        when (issuerRole) {
            Role.PASSENGER -> {
                val driverId = db.getWay(newLift.driverWayId)!!.userId
                if (oldStatus == Lift.Status.SUGGESTED) {
                    val liftInfo = LiftInfo(
                            newLift,
                            role = Role.DRIVER,
                            partnerWay = db.getWay(newLift.passengerWayId)!!,
                            partner = db.getUser(issuerId)!!)
                    val message = LiftInfoMessage(newLift.driverWayId,ArrayList(setOf(liftInfo)))
                    messenger.send(driverId, message)
                } else {
                    val message = LiftStatusMessage(
                            referenceWayId = newLift.driverWayId,
                            liftId = newLift.id,
                            status = newStatus)
                    messenger.send(driverId, message)
                }
            }
            Role.DRIVER -> {
                val passengerId = db.getWay(newLift.passengerWayId)!!.userId
                val message = LiftStatusMessage(
                        referenceWayId = newLift.passengerWayId,
                        liftId = newLift.id,
                        status = newStatus)
                messenger.send(passengerId, message)
            }
        }
    }

    override fun onRemoveLift(lift: Lift) {
        when (lift.status) {
            Lift.Status.FINISHED, Lift.Status.DRIVER_CANCELED -> {
                val message = LiftRemoveMessage(referenceWayId=lift.passengerWayId,liftId=lift.id)
                messenger.send(db.getWay(lift.passengerWayId)!!.userId, message)
            }
            Lift.Status.PASSENGER_CANCELED -> {
                val message = LiftRemoveMessage(referenceWayId = lift.driverWayId, liftId = lift.id)
                messenger.send(db.getWay(lift.driverWayId)!!.userId, message)
            }
            else -> {
                logger.error("Lift removed in unexpected status " + lift.status.name)
            }
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(HitchResource::class.java)
    }

    init {
        db.addListener(this)
        // FIXME need proper user administration
        db.putUser( User("max", "Max", "Kubierschky", "Max Kubierschky", "0151 26019215"))
        db.putUser( User("moritz", "Moritz", "Bleibtreu", "Der Moritz", null) )
        db.putUser( User("hans", "Hans", "Dampf", "Hans Dampf", null) )
        logger.debug("Initializing db "+(RamHitchDB.debugCounter++))
    }
}