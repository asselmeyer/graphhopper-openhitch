package de.wifaz.oh.server

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.math.max

class BasePriceModel(
        override val currency: Currency,
        private val minimumPrice: Double,
        private val pricePerSharedKm: Double,
        private val pricePerDeviationKm: Double
) : PriceModel {

    override fun getPrice(sharedDistance: Double, deviationDistance: Double): BigDecimal {
        var price:Double = pricePerSharedKm * sharedDistance / 1000 + pricePerDeviationKm * deviationDistance / 1000
        price = max(price, minimumPrice)
        return BigDecimal(price).setScale(currency.defaultFractionDigits, RoundingMode.HALF_UP)
    }

}